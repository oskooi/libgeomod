#include <geomod/planar/rectangle.h>
#include <geomod/planar/polygon.h>
#include <geomod/volume/extrusion.h>
#include <geomod/surface/mesh.h>
#include <iostream>

using geomod::coord_type;
using geomod::affine::Point3;
using geomod::affine::Vector3;

int main(int argc, char *argv[]){
	geomod::planar::Rectangle r(1, 2);
	geomod::volume::Extrusion e(&r, Vector3(0.1, 0.2, 1));
	geomod::surface::Mesh *m = e.AsMesh();
	
	geomod::surface::Mesh::vertex_vector vertices;
	geomod::surface::Mesh::index_vector triangle_indices;
	m->GetElements(vertices, triangle_indices);
	const size_t nv = vertices.size();
	const size_t nt = triangle_indices.size() / 3;
	for(size_t i = 0; i < nv; ++i){
		std::cout << "v[" << i << "] = " << vertices[i] << std::endl;
	}
	for(size_t i = 0; i < nt; ++i){
		std::cout << "t[" << i << "] = ("
			<< triangle_indices[3*i+0] << ", "
			<< triangle_indices[3*i+1] << ", "
			<< triangle_indices[3*i+2] << ")\n";
	}
	return 0;
}
