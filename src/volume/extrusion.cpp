#include <geomod/volume/extrusion.h>
#include <geomod/util/specfun.h>
#include <geomod/planar/polygon.h>
#include <geomod/surface/mesh.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::volume;


Point3 Extrusion::BaseParameterization::operator()(const Point2 &uv) const{
	Point3 p(uv[0], uv[1], 0);
	if(!z1){
		return p;
	}else{
		return p + e->offset();
	}
}
Point2 Extrusion::BaseParameterization::operator()(const Point3 &r) const{
	if(!z1){
		return Point2(r[0], r[1]);
	}else{
		const Vector3 &h = e->offset();
		return Point2(r[0] - h[0], r[1] - h[1]);
	}
}
bool Extrusion::BaseParameterization::RayIntersection(const Ray3 &r, std::vector<Ray3::Intersection> &x) const{
	// Ray-plane intersection:
	//   p[2] + t*d[2] == h[2]
	const Vector3 h(z1 ? e->offset() : Vector3::Zero());
	const Point3  &rp = r.origin();
	const Vector3 &rd = r.dir();
	const coord_type t = (h[2]-rp[2]) / rd[2];
	if(t <= 0){ return false; }
	Point2 p(rp[0] + t*rd[0] - h[0], rp[1] + t*rd[1] - h[1]);
	if(e->base()->Contains(p)){
		Ray3::Intersection xi;
		xi.t = t;
		xi.uv = p;
		xi.p = r(t);
		x.push_back(xi);
		return true;
	}
	return false;
}
void Extrusion::BaseParameterization::Neighborhood(const Point2 &uv, DifferentialGeometry &dg) const{
	const coord_type hz(e->offset()[2]);
	dg.dpdu = Vector3::X();
	dg.dpdv = Vector3::Y();
	dg.II[0] = 0;
	dg.II[1] = 0;
	dg.II[2] = 0;
	if((hz > 0 && !z1) || (hz < 0 && z1)){
		dg.orientation = -1;
	}else{
		dg.orientation = 1;
	}
}
AABB2 Extrusion::BaseParameterization::Bounds() const{
	return e->base()->Bounds();
}
surface::Parameterization::uv_boundary Extrusion::BaseParameterization::GetBoundary(const coord_type &tol) const{
	planar::Polygon *poly = e->base()->AsPolygon(tol);
	surface::Parameterization::uv_boundary b(poly->GetVertices());
	delete poly;
	return b;
}

Point3 Extrusion::EdgeParameterization::operator()(const Point2 &uv) const{
	const Vector3 &h = e->offset();
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	const Point2 p((*param)(uv[0]));
	return Point3(p[0] + uv[1]*h[0], p[1] + uv[1]*h[1], uv[1]*h[2]);
}
Point2 Extrusion::EdgeParameterization::operator()(const Point3 &r) const{
	const Vector3 &h = e->offset();
	const coord_type v(specfun::clamp(r[2] / h[2]));
	const Point2 p(r[0] - v*h[0], r[1] - v*h[1]);
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	return Point2((*param)(p), v);
}
bool Extrusion::EdgeParameterization::RayIntersection(const Ray3 &r, std::vector<Ray3::Intersection> &x) const{
	const Vector3 &h = e->offset();
	const Point3  &p3 = r.origin();
	const Vector3 &d3 = r.dir();
	// Perform 2D intersection on de-skewed ray.
	// Shearing transform:
	//   [ 1   a ] [ x ]   [ x' ]
	//   [   1 b ] [ y ] = [ y' ]
	//   [     1 ] [ z ]   [ z  ]
	// We want to shear the h vector to be vertical, so a = -h[0]/h[2], b = -h[1]/h[2]
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	Point2 p2(
		p3[0] - h[0]/h[2]*p3[2],
		p3[1] - h[1]/h[2]*p3[2]
	);
	Vector2 d2(
		d3[0] - h[0]/h[2]*d3[2],
		d3[1] - h[1]/h[2]*d3[2]
	);
	Ray2 r2(p2, d2);
	std::vector<Ray2::Intersection> x2;
	if(!param->RayIntersection(r2, x2)){ return false; }
	for(std::vector<Ray2::Intersection>::const_iterator xi = x2.begin(); x2.end() != xi; ++xi){
		const coord_type v = (p3[2] + xi->t * d3[2]) / h[2];
		if(0 < v && v < 1){
			Ray3::Intersection x3;
			x3.t = xi->t;
			x3.uv = Point2(xi->s, v);
			x3.p = r(x3.t);
			x.push_back(x3);
		}
	}
	return x.size() > 0;
}
void Extrusion::EdgeParameterization::Neighborhood(const Point2 &uv, DifferentialGeometry &dg) const{
	const onedim::Parameterization *param = e->base()->GetChartParameterization(ichart);
	onedim::Parameterization::DifferentialGeometry dg1;
	param->Neighborhood(uv[0], dg1);
	dg.dpdu = Vector3(dg1.dp[0], dg1.dp[1], 0);
	dg.dpdv = e->offset();

	Vector3 n(Vector3::Cross(dg.dpdu, dg.dpdv));
	n.Normalize();
	dg.II[0] = dg1.ddp[0]*n[0] + dg1.ddp[1]*n[1];
	dg.II[1] = 0;
	dg.II[2] = 0;
	dg.orientation = 1;
}
AABB2 Extrusion::EdgeParameterization::Bounds() const{
	return AABB2(Point2(0, 0), Point2(1, 1));
}
surface::Parameterization::uv_boundary Extrusion::EdgeParameterization::GetBoundary(const coord_type &tol) const{
	surface::Parameterization::uv_boundary b;
	b.reserve(4);
	b.push_back(Point2(0, 0));
	b.push_back(Point2(1, 0));
	b.push_back(Point2(1, 1));
	b.push_back(Point2(0, 1));
	return b;
}


Extrusion::Extrusion(const Extrusion::region_type *region, const Extrusion::vector_type &v):
	reg(region->Clone()),
	h(v),
	param0(this, false),
	param1(this, true)
{
	// Generate parameterizations
	parame.reserve(reg->NumCharts());
	for(size_t ichart = 0; ichart < reg->NumCharts(); ++ichart){
		parame.push_back(EdgeParameterization(this, ichart));
	}
}
Extrusion::~Extrusion(){
	delete reg;
}

coord_type Extrusion::SurfaceArea() const{
	return 2*reg->Area() + h.Norm()*reg->Perimeter();
}
coord_type Extrusion::Volume() const{
	return reg->Area() * h[2];
}
Point3 Extrusion::Centroid() const{
	Point2 c(reg->Centroid());
	return Point3(c[0] + 0.5*h[0], c[1] + 0.5*h[1], 0.5*h[2]);
}

Point3 Extrusion::Extremum(const Vector3 &dir) const{
	Vector2 v(dir[0], dir[1]);
	Point2 p(reg->Extremum(v));
	Vector3 q(p[0], p[1], 0);
	if(Vector3::Dot(dir, q) > Vector3::Dot(dir, q+h)){
		return Point3::Origin() + q;
	}else{
		return Point3::Origin() + q + h;
	}
}
Point3 Extrusion::Nearest(const Point3 &p) const{
	Point2 q(reg->Nearest(Point2(p[0], p[1])));
	Point3 r(q[0], q[1], 0);
	if((r-p).NormSq() < (r+h-p).NormSq()){
		return r;
	}else{
		return r + h;
	}
}
AABB3 Extrusion::Bounds() const{
	AABB2 b(reg->Bounds());
	Point3 mn(b.Vertex(0)[0], b.Vertex(0)[1], 0);
	Point3 mx(b.Vertex(3)[0], b.Vertex(3)[1], 0);
	Point3 m1(b.Vertex(1)[0], b.Vertex(1)[1], 0);
	Point3 m2(b.Vertex(2)[0], b.Vertex(2)[1], 0);
	AABB3 c(mn, mx);
	c += mn + h;
	c += mx + h;
	c += m1 + h;
	c += m2 + h;
	return c;
}

size_t Extrusion::NumCharts() const{
	return 2 + reg->NumCharts();
}
const surface::Parameterization* Extrusion::GetChartParameterization(index_type ichart) const{
	if(0 == ichart){ return &param0; }
	if(1 == ichart){ return &param1; }
	return &parame[ichart-2];
}
surface::Parameterizable::chart_outline Extrusion::GetChartOutline(index_type ichart, const coord_type &tol) const{
	surface::Parameterizable::chart_outline outline;
	const planar::Polygon *poly = reg->AsPolygon(tol);
	size_t n = poly->size();
	if(0 == ichart || 1 == ichart){
		outline.reserve(n);
		if(0 == ichart){
			while(n --> 0){
				const Point2 &v = poly->vertex(n);
				outline.push_back(Point3(v[0], v[1], 0));
			}
		}else{
			for(size_t i = 0; i < n; ++i){
				const Point2 &v = poly->vertex(n);
				outline.push_back(Point3(v[0]+h[0], v[1]+h[1], h[2]));
			}
		}
	}else{
		ichart -= 2;
	}
	delete poly;
	return outline;
}

Extrusion* Extrusion::Clone() const{ return new Extrusion(reg, h); }

bool Extrusion::Contains(const Point3 &p) const{
	if(p[2] < 0 || p[2] > h[2]){ return false; }
	coord_type t = p[2] / h[2];
	Point2 q(p[0] + t*h[0], p[1] + t*h[1]);
	return reg->Contains(q);
}

surface::Mesh* Extrusion::AsMesh(const coord_type &tol) const{
	const planar::Polygon* poly = reg->AsPolygon(tol);
	const planar::Polygon::vertex_vector& poly_vertices = poly->GetVertices();
	const size_t nv = poly_vertices.size();

	surface::Mesh::vertex_vector v;
	// Add all vertices
	v.reserve(2*nv);
	for(size_t i = 0; i < nv; ++i){
		v.push_back(Point3(
			poly_vertices[i][0] + h[0],
			poly_vertices[i][1] + h[1],
			h[2]
		));
	}
	for(size_t i = 0; i < nv; ++i){
		v.push_back(Point3(
			poly_vertices[i][0],
			poly_vertices[i][1],
			0
		));
	}

	surface::Mesh::index_vector ind;
	poly->Triangulate(ind);
	const size_t nt = ind.size() / 3;

	// Add top and bottom triangle indices
	// Top vertices came first, so we just need to append bottoms and flip
	ind.reserve(6*nt + 6*nv);
	for(size_t i = 0; i < nt; ++i){
		ind.push_back(ind[3*i+0]+nv);
		ind.push_back(ind[3*i+2]+nv);
		ind.push_back(ind[3*i+1]+nv);
	}
	for(size_t i = nv-1, j = 0; j < nv; i=j++){
		ind.push_back(j);
		ind.push_back(i);
		ind.push_back(i+nv);

		ind.push_back(i+nv);
		ind.push_back(j+nv);
		ind.push_back(j);
	}

	delete poly;
	return new surface::Mesh(v, ind);
}

Solid* Extrusion::Offset(const coord_type &dist, const coord_type &tol) const{
	return NULL; // TODO
}

bool Extrusion::Convex() const{
	return reg->Convex();
}