#include <geomod/volume/sphere.h>
#include <geomod/util/specfun.h>
#include <geomod/util/polynomial.h>
#include <geomod/surface/mesh.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::volume;

// Parameterization in angular spherical coordinates:
// phi in [0, pi] from +z pole, and theta in [0, 2pi] around +z pole.

Point3 Sphere::Parameterization::operator()(const Point2 &uv) const{
	const coord_type &r = s->radius();
	coord_type cs_phi, sn_phi, cs_theta, sn_theta;
	specfun::cossin(uv[0], &cs_phi, &sn_phi);
	specfun::cossin(uv[1], &cs_theta, &sn_theta);
	return Point3(r*cs_theta*sn_phi, r*sn_theta*sn_phi, r*cs_phi);
}
Point2 Sphere::Parameterization::operator()(const Point3 &p) const{
	Vector3 v(p[0], p[1], p[2]);
	v.Normalize();
	coord_type theta = atan2(v[1], v[0]);
	coord_type phi = atan2(hypot(v[1], v[0]), v[2]);
	if(theta < 0){ theta += 2*M_PI; }
	if(phi < 0){ phi += 2*M_PI; }
	return Point2(phi, theta);
}
bool Sphere::Parameterization::RayIntersection(const Ray3 &ray, std::vector<Ray3::Intersection> &x) const{
	const coord_type &r = s->radius();
	// We need to solve |p + t * d|^2 == r^2
	//   d.d t^2 + 2 p.d t + p.p-r*r == 0
	Vector3 p(ray.origin() - Point3::Origin());
	coord_type rt[2];
	unsigned nroots = polynomial::quadratic_equation(
		ray.dir().NormSq(),
		Vector3::Dot(ray.dir(), p),
		p.NormSq() - r*r,
		rt
	);
	if(nroots > 2){
		if(rt[0] > rt[1]){ std::swap(rt[0], rt[1]); }
	}
	for(unsigned i = 0; i < nroots; ++i){
		if(rt[i] <= 0){ continue; }
		Ray3::Intersection xi;
		xi.t = rt[i];
		xi.p = ray(xi.t);
		xi.uv = (*this)(xi.p);
		x.push_back(xi);
	}
	return x.size() > 0;
}
void Sphere::Parameterization::Neighborhood(const Point2 &uv, DifferentialGeometry &dg) const{
	const coord_type &r = s->radius();
	coord_type cs_phi, sn_phi, cs_theta, sn_theta;
	specfun::cossin(uv[0], &cs_phi, &sn_phi);
	specfun::cossin(uv[1], &cs_theta, &sn_theta);
	
	dg.dpdu = Vector3(r*cs_theta*cs_phi, r*sn_theta*cs_phi, -r*sn_phi);
	dg.dpdv = Vector3(-r*sn_theta*sn_phi, r*cs_theta*sn_phi, 0);
	dg.II[0] = -r;
	dg.II[1] = 0;
	dg.II[2] = -r*sn_phi*sn_phi;
	dg.orientation = 1;
}
AABB2 Sphere::Parameterization::Bounds() const{
	return AABB2(Point2(0, 0), Point2(M_PI, 2*M_PI));
}
surface::Parameterization::uv_boundary Sphere::Parameterization::GetBoundary(const coord_type &tol) const{
	surface::Parameterization::uv_boundary b;
	b.reserve(4);
	b.push_back(Point2(   0,      0));
	b.push_back(Point2(M_PI,      0));
	b.push_back(Point2(M_PI, 2*M_PI));
	b.push_back(Point2(   0, 2*M_PI));
	return b;
}

coord_type Sphere::SurfaceArea() const{
	return 4*M_PI*r*r;
}
coord_type Sphere::Volume() const{
	return (4*M_PI/3)*r*r*r;
}
Point3 Sphere::Centroid() const{
	return Point3::Origin();
}

Point3 Sphere::Extremum(const Vector3 &dir) const{
	Vector3 v(dir);
	v.Normalize(r);
	return Point3::Origin() + v;
}
Point3 Sphere::Nearest(const Point3 &p) const{
	Vector3 v(p - Point3::Origin());
	v.Normalize(r);
	return Point3::Origin() + v;
}
AABB3 Sphere::Bounds() const{
	return AABB3(Point3(-r, -r, -r), Point3(r, r, r));
}

Sphere* Sphere::Clone() const{ return new Sphere(r); }

bool Sphere::Contains(const Point3 &p) const{
	return (p - Point3::Origin()).Norm() < r;
}

size_t Sphere::NumCharts() const{ return 1; }
const surface::Parameterization* Sphere::GetChartParameterization(index_type ichart) const{
	return &param;
}

index_type Sphere::RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
	std::vector<affine::Ray3::Intersection> xv;
	GetChartParameterization(0)->RayIntersection(r, xv);
	if(xv.size() > 0){
		x = xv.front();
		return 0;
	}
	return -1;
}

surface::Mesh* Sphere::AsMesh(const coord_type &tol) const{
	unsigned int niters = 4; // TODO: compute this based on tol

	std::vector<Vector3> v(4);
	v[0] = Vector3::Normalize(Vector3( 1,  1,  1));
	v[1] = Vector3::Normalize(Vector3( 1, -1, -1));
	v[2] = Vector3::Normalize(Vector3(-1,  1, -1));
	v[3] = Vector3::Normalize(Vector3(-1, -1,  1));

	surface::Mesh::index_vector t(12);
	t[0] = 0;
	t[1] = 1;
	t[2] = 2;

	t[3] = 0;
	t[4] = 3;
	t[5] = 1;

	t[6] = 0;
	t[7] = 2;
	t[8] = 3;

	t[ 9] = 1;
	t[10] = 3;
	t[11] = 2;

	unsigned ntri = 4;
	while(niters --> 0){
		// Subdivide
		for(unsigned itri = 0; itri < ntri; ++itri){
			const index_type ia = t[3*itri+0];
			const index_type ib = t[3*itri+1];
			const index_type ic = t[3*itri+2];
			const Vector3 &a = v[ia];
			const Vector3 &b = v[ib];
			const Vector3 &c = v[ic];
			const Vector3 ab = Vector3::Normalize(0.5*(a+b));
			const Vector3 bc = Vector3::Normalize(0.5*(b+c));
			const Vector3 ca = Vector3::Normalize(0.5*(c+a));
			const index_type iab = v.size(); v.push_back(ab);
			const index_type ibc = v.size(); v.push_back(bc);
			const index_type ica = v.size(); v.push_back(ca);
			t.push_back(ia); t.push_back(iab); t.push_back(ica);
			t.push_back(ib); t.push_back(ibc); t.push_back(iab);
			t.push_back(ic); t.push_back(ica); t.push_back(ibc);
			t[3*itri+0] = iab;
			t[3*itri+1] = ibc;
			t[3*itri+2] = ica;
		}
		ntri *= 4;
	}
	surface::Mesh::vertex_vector p;
	p.reserve(v.size());
	for(size_t i = 0; i < v.size(); ++i){
		p.push_back(surface::Mesh::vertex_type::Origin() + r*v[i]);
	}
	return new surface::Mesh(p, t);
}

Sphere* Sphere::Offset(const coord_type &dist, const coord_type &tol) const{
	return new Sphere(r + dist);
}