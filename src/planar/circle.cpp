#include <geomod/planar/circle.h>
#include <geomod/onedim/parameterization.h>
#include <geomod/util/specfun.h>
#include <geomod/util/polynomial.h>
#include <geomod/planar/polygon.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

Circle::Parameterization::Parameterization(const Circle *parent):c(parent){}

Point2 Circle::Parameterization::operator()(const coord_type &u) const{
	coord_type cs, sn;
	specfun::cossin(2*M_PI*u, &cs, &sn);
	return Point2(c->radius()*cs, c->radius()*sn);
}

coord_type Circle::Parameterization::operator()(const Point2 &p)  const{
	coord_type u = atan2(p[1], p[0]) / (2*M_PI);
	if(u < 0){ u += 1; }
	return u;
}

bool Circle::Parameterization::RayIntersection(const Ray2 &ray, std::vector<Ray2::Intersection> &x) const{
	const coord_type r = c->radius();
	// Solve |p + t*d|^2 == r^2
	const Vector2 vp(ray.origin() - Point2::Origin());
	const coord_type a = ray.dir().NormSq();
	const coord_type b = Vector2::Dot(vp, ray.dir());
	const coord_type c = vp.NormSq() - r*r;
	coord_type roots[2];
	unsigned nroots = polynomial::quadratic_equation(a, b, c, roots);
	if(0 == nroots){ return false; }
	if(nroots > 1){
		if(roots[0] > roots[1]){
			std::swap(roots[0], roots[1]);
		}
	}
	for(unsigned i = 0; i < nroots; ++i){
		if(roots[i] > 0){
			Ray2::Intersection xi;
			xi.t = roots[i];
			xi.p = ray(xi.t);
			xi.s = (*this)(xi.p);
			x.push_back(xi);
		}
	}
	return true;
}

void Circle::Parameterization::Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const{
	const coord_type r = c->radius();
	static const coord_type tau(2*M_PI);
	coord_type cs, sn;
	specfun::cossin(tau*u, &cs, &sn);
	dg.dp = Vector2(-tau*r*sn, tau*r*cs);
	dg.ddp = Vector2(-tau*tau*r*cs, -tau*tau*r*sn);
}



bool Circle::Contains(const Point2 &p) const{
	return (p - Point2::Origin()).Norm() < r;
}

Point2 Circle::Extremum(const Vector2 &dir) const{
	Vector2 v(dir);
	v.Normalize(r);
	return Point2::Origin() + v;
}
Point2 Circle::Nearest(const Point2 &p) const{
	Vector2 v(p - Point2::Origin());
	v.Normalize(r);
	return Point2::Origin() + v;
}
AABB2 Circle::Bounds() const{
	return AABB2(Point2(-r, -r), Point2(r, r));
}
	
coord_type Circle::Perimeter() const{ return 2*M_PI*r; }
coord_type Circle::Area() const{ return M_PI*r*r; }
Point2 Circle::Centroid() const{ return Point2::Origin(); }

complex_type Circle::FourierTransform(const Vector2 &f) const{
	return Area() * specfun::FourierJinc(r * f.Norm());
}
	
Polygon* Circle::AsPolygon(const coord_type &tol) const{
	// Compute angle theta such that tol/r = 1 - cos(theta/2)
	// Then nsegments = ceil(2pi/angle)
	// We use the 4th order Taylor series,
	//   tol/r =~ theta^2/8 - theta^4/96
	// where the RHS is an underestimate.
	const coord_type rtol8 = 8*tol/r;
	coord_type theta2 = rtol8; // 2nd order Taylor approximation for theta^2
	// Perform 1 step of Newton correction
	coord_type qq = theta2*theta2/coord_type(6);
	theta2 -= 0.5*qq / (qq - 1); // One Newton correction
	const coord_type theta = std::sqrt(theta2);
	const size_t n = (int)ceil(2*M_PI / theta);

	coord_type cs1, sn1;
	specfun::cossin(2*M_PI / n, &cs1, &sn1);

	Polygon::vertex_vector v;
	v.reserve(n);
	coord_type cs(1), sn(0);
	for(size_t i = 0; i < n; ++i){
		v.push_back(Point2(r*cs, r*sn));
		coord_type temp = cs;
		cs = cs1*cs - sn1*sn;
		sn = cs1*sn + sn1*temp;
	}
	return new Polygon(v);
}

onedim::Parameterizable::chart_polyline Circle::GetChartPolyline(index_type ichart, const coord_type &tol) const{
	Polygon *p = AsPolygon(tol);
	onedim::Parameterizable::chart_polyline poly(p->GetVertices());
	delete p;
	poly.push_back(poly.front());
	return poly;
}

Circle* Circle::Offset(const coord_type &dist, const coord_type &tol) const{
	return new Circle(r + dist);
}

void Circle::GetSamples(size_t n, Region::SamplingSpec spec, std::vector<point_type> &points) const{
	// Generate a hexapolar grid.
	points.push_back(Point2::Origin());
	if(n < 7){ return; }
	// First "ring" contains 1 point, next contains 6, 12, 18, etc.

	// Determine number of rings; find smallest m such that:
	//   1 + 6*Sum[i, {i, 1, m}] <= n
	// I'm sure we can determine this with an equation, but this is fine for now.
	int m = 0;
	{
		int nn = 1;
		while(nn <= n){
			m++;
			nn += 6*m;
		}
		nn -= 6*m;
		m--; // we overshot by one, so go back.
		points.reserve(nn);
	}

	const coord_type dr = r / (m+0.5);
	for(int ir = 1; ir <= m; ++ir){ // which ring
		const coord_type radius = ir*dr;
		const int nq = 6*ir;
		const coord_type dq = 2*M_PI / nq;
		const coord_type cs1 = cos(dq);
		const coord_type sn1 = sin(dq);
		coord_type cs = 1, sn = 0;
		for(int iq = 0; iq < nq; ++iq){
			points.push_back(point_type(radius * cs, radius * sn));
			// Advance angle
			const coord_type cc = cs;
			cs = cs1*cc - sn1*sn;
			sn = cs1*sn + sn1*cc;
		}
	}
}
