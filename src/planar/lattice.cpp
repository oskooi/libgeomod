#include <geomod/planar/lattice.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

Lattice::Lattice(const coord_type &period){
	L[0] = period;
	L[1] = 0;
	L[2] = 0;
	L[3] = 0;
}

Lattice::Lattice(const Vector2 &u, const Vector2 &v){
	L[0] = u[0];
	L[1] = u[1];
	L[2] = v[0];
	L[3] = v[1];
}

unsigned Lattice::Dimensionality() const{
	if(0 == L[2] && 0 == L[3]){ return 1; }
	else{ return 2; }
}

coord_type Lattice::Volume() const{
	if(1 == Dimensionality()){
		return hypot(L[0], L[1]);
	}else{
		return L[0]*L[3] - L[1]*L[2];
	}
}

Lattice Lattice::Reciprocal() const{
	if(1 == Dimensionality()){
		const coord_type idenom = 1 / (L[0]*L[0] + L[1]*L[1]);
		return Lattice(Vector2(L[0] * idenom, L[1] * idenom));
	}else{
		const coord_type idenom = 1 / (L[0]*L[3] - L[1]*L[2]);
		return Lattice(
			Vector2( L[3] * idenom, -L[2] * idenom),
			Vector2(-L[1] * idenom,  L[0] * idenom)
		);
	}
}

static coord_type reduce_scalar(coord_type t){
	coord_type intpart;
	t = modf(t, &intpart);
	if(t >= 0.5){ t -= 1; }
	if(t < -0.5){ t += 1; }
	return t;
}

Point2 Lattice::Reduce(const Point2 &p) const{
	if(1 == Dimensionality()){
		const Vector2 u(L[0], L[1]);
		const coord_type s = Vector2::Dot(p - Point2::Origin(), u) / u.NormSq();
		const coord_type s2 = reduce_scalar(s);
		return p - (s2 - s)*u;
	}else{
		const coord_type idenom = 1 / (L[0]*L[3] - L[1]*L[2]);
		coord_type s = (L[3] * p[0] - L[2] * p[1]) * idenom;
		coord_type t = (L[0] * p[1] - L[1] * p[0]) * idenom;
		s = reduce_scalar(s);
		t = reduce_scalar(t);
		return Point2(
			s * L[0] + t * L[2],
			s * L[1] + t * L[3]
		);
	}
}
	
Vector2 Lattice::operator()(index_type iu, index_type iv) const{
	return Vector2(
		iu * L[0] + iv * L[2],
		iu * L[1] + iv * L[3]
	);
}

void Lattice::SelectShortest(size_t n, std::vector<index_pair> &indices) const{
	if(1 == Dimensionality()){
		// Round n down to nearest odd number, and select symmetrically about 0.
		if(n < 1){ n = 1; }
		if(0 == (n&1)){ n--; }
		indices.reserve(n);
		indices.push_back(index_pair(0,0));
		const size_t h = n/2;
		for(size_t i = 1; i <= h; ++i){
			indices.push_back(index_pair( i,0));
			indices.push_back(index_pair(-i,0));
		}
	}else{
		// The shortest lattice vectors lie in a circle about the origin.
		// We want to determine the smallest indices in each direction to
		// guarantee that the parallelogram spanning those indices completely
		// covers the circle.
		// Thus, we must find the minimum values of hu and hv such that
		//   |(hu u + hv v) . rot(u)/|u|| > r and |(hu u + hv v) . rot(v)/|v|| > r
		// Or,
		//   |hv v x u|/|u| > r and |hu u x v|/|v| > r
		// Finally, the choice of r depends on the volume of the unit cell;
		// for large n, the area of the circle is roughly pi r^2 = n (u x v).
		// We will overestimate:
		coord_type x = std::abs(L[0]*L[3] - L[1]*L[2]);
		coord_type r = 1.5*sqrt((coord_type)n * x/3.14);
		int h[2] = {
			(int)(r * hypot(L[0], L[1]) / x) + 1,
			(int)(r * hypot(L[2], L[3]) / x) + 1
		};
		const int nsuper = (2*h[0]+1)*(2*h[1]+1); // size of superset
		std::vector<coord_type> len;
		indices.reserve(nsuper);
		len.reserve(nsuper);
		for(int i = -h[0]; i <= h[0]; ++i){
			for(int j = -h[1]; j <= h[1]; ++j){
				const coord_type lij = hypot(L[0]*i + L[2]*j, L[1]*i + L[3]*j);
				indices.push_back(index_pair(i,j));
				len.push_back(lij);
			}
		}
		// Sort into ascending order by len
		for(int j = 0; j+1 < nsuper; ++j){
			int imin = j;
			for(int i = j+1; i < nsuper; ++i){
				if(len[i] < len[imin]){ imin = i; }
			}
			if(imin != j){
				std::swap(len[j], len[imin]);
				std::swap(indices[j], indices[imin]);
			}
		}
		// Scan backwards until we find a change in len, and then truncate to that point.
		const coord_type tol = std::numeric_limits<coord_type>::epsilon() * std::sqrt(x);
		while(n > 1){
			if(std::abs(len[n-1] - len[n]) > tol){ break; }
			--n;
		}
		indices.resize(n);
	}
}
