#include <geomod/planar/rectangle.h>
#include <geomod/onedim/parameterization.h>
#include <geomod/util/specfun.h>
#include <geomod/planar/polygon.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

Point2 Rectangle::EdgeParameterization::operator()(const coord_type &u) const{
	return a+u*v;
}
coord_type Rectangle::EdgeParameterization::operator()(const affine::Point2 &p) const{
	coord_type t = Vector2::Dot(p-a, v) / v.NormSq();
	if(t < 0){ t = 0; }
	if(t > 1){ t = 1; }
	return t;
}
bool Rectangle::EdgeParameterization::RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const{
	Ray2::Intersection xsect;
	if(Ray2(a, v).Intersect(ray, xsect) && xsect.t > 0 && 0 <= xsect.s && xsect.s < 1){
		x.push_back(xsect);
		return true;
	}
	return false;
}
void Rectangle::EdgeParameterization::Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const{
	dg.dp = v;
	dg.ddp = Vector2::Zero();
}

Point2 Rectangle::vertex(index_type i) const{
	switch(i & 0x3){
	case 0:
		return Point2(-h[0], -h[1]);
	case 1:
		return Point2( h[0], -h[1]);
	case 2:
		return Point2( h[0],  h[1]);
	default:
		return Point2(-h[0],  h[1]);
	}
}

bool Rectangle::Contains(const Point2 &p) const{
	return std::abs(p[0]) < h[0] && std::abs(p[1]) < h[1];
}

Point2 Rectangle::Extremum(const Vector2 &dir) const{
	unsigned ibest = 0;
	coord_type dbest = Vector2::Dot(vertex(0) - Point2::Origin(), dir);
	for(unsigned i = 1; i < 4; ++i){
		coord_type d = Vector2::Dot(vertex(i) - Point2::Origin(), dir);
		if(d > dbest){
			ibest = i;
			dbest = d;
		}
	}
	return vertex(ibest);
}
Point2 Rectangle::Nearest(const Point2 &p_) const{
	return vertex(0); // TODO
}
AABB2 Rectangle::Bounds() const{
	return AABB2(Point2(-h[0], -h[1]), Point2(h[0], h[1]));
}


coord_type Rectangle::Perimeter() const{ return 2*(h[0] + h[1]); }
coord_type Rectangle::Area() const{ return 4*h[0]*h[1]; }
Point2 Rectangle::Centroid() const{ return Point2::Origin(); }

complex_type Rectangle::FourierTransform(const Vector2 &f) const{
	return Area() * specfun::FourierSinc(2*h[0] * f[0]) * specfun::FourierSinc(2*h[1] * f[1]);
}
	
Polygon* Rectangle::AsPolygon(const coord_type &tol) const{
	Polygon::vertex_vector v;
	v.reserve(4);
	for(size_t i = 0; i < 4; ++i){
		v.push_back(vertex(i));
	}
	return new Polygon(v);
}

onedim::Parameterizable::chart_polyline Rectangle::GetChartPolyline(index_type ichart, const coord_type &tol) const{
	onedim::Parameterizable::chart_polyline poly;
	poly.reserve(2);
	switch(ichart & 0x3){
	case 0:
		poly.push_back(Point2(-h[0], -h[1]));
		poly.push_back(Point2( h[0], -h[1]));
		break;
	case 1:
		poly.push_back(Point2( h[0], -h[1]));
		poly.push_back(Point2( h[0],  h[1]));
		break;
	case 2:
		poly.push_back(Point2( h[0],  h[1]));
		poly.push_back(Point2(-h[0],  h[1]));
		break;
	default:
		poly.push_back(Point2(-h[0],  h[1]));
		poly.push_back(Point2(-h[0], -h[1]));
		break;
	}
	return poly;
}

Rectangle* Rectangle::Offset(const coord_type &dist, const coord_type &tol) const{
	return new Rectangle(h[0] + dist, h[1] + dist);
}

void Rectangle::GetSamples(size_t n, Region::SamplingSpec spec, std::vector<point_type> &points) const{
	//const coord_type aspect = h[1] / h[0];
	const coord_type delta = std::sqrt(4*h[0]*h[1] / n);
	const int nx = (int)(2*h[0] / delta);
	const int ny = (int)(2*h[1] / delta);
	
	points.reserve(nx*ny);
	for(int iy = 0; iy < ny; ++iy){
		const coord_type ty = (iy + 0.5) / ny - 0.5;
		const coord_type y = 2*h[1] * ty;
		for(int ix = 0; ix < nx; ++ix){
			const coord_type tx = (ix + 0.5) / nx - 0.5;
			const coord_type x = 2*h[0] * tx;
			points.push_back(point_type(x, y));
		}
	}
}
