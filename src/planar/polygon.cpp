#include <geomod/onedim/parameterization.h>
#include <geomod/planar/polygon.h>
#include <geomod/planar/rectangle.h>
#include <geomod/util/specfun.h>

using namespace geomod;
using namespace geomod::affine;
using namespace geomod::planar;

Point2 Polygon::EdgeParameterization::operator()(const coord_type &u) const{
	return a+u*v;
}
coord_type Polygon::EdgeParameterization::operator()(const affine::Point2 &p) const{
	coord_type t = Vector2::Dot(p-a, v) / v.NormSq();
	if(t < 0){ t = 0; }
	if(t > 1){ t = 1; }
	return t;
}
bool Polygon::EdgeParameterization::RayIntersection(const affine::Ray2 &ray, std::vector<affine::Ray2::Intersection> &x) const{
	Ray2::Intersection xsect;
	if(Ray2(a, v).Intersect(ray, xsect) && xsect.t > 0 && 0 <= xsect.s && xsect.s < 1){
		x.push_back(xsect);
		return true;
	}
	return false;
}
void Polygon::EdgeParameterization::Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const{
	dg.dp = v;
	dg.ddp = Vector2::Zero();
}


const Point2& Polygon::vertex(index_type i) const{
	index_type n = v.size();
	if(i < 0){ i += n; }
	else if(i >= n){ i -= n; }
	return v[i];
}

bool Polygon::Contains(const Point2 &p) const{
	// This is the PNPOLY algorithm of W. Randolph Franklin:
	// https://wrfranklin.org/Research/Short_Notes/pnpoly.html
	bool c = false;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		if(
			((v[j][1] > p[1]) != (v[i][1] > p[1])) &&
			(p[0] < (v[i][0]-v[j][0]) * (p[1]-v[j][1]) / (v[i][1]-v[j][1]) + v[j][0])
		){
			c = !c;
		}
	}
	return c;
}

bool Polygon::Convex() const{
	for(size_t i = v.size()-2, j = v.size()-1, k = 0; k < v.size(); i = j, j = k++){
		if(Point2::Orient(v[i], v[j], v[k]) < 0){
			return false;
		}
	}
	return true;	
}

Point2 Polygon::Extremum(const Vector2 &dir) const{
	size_t j = 0;
	for(size_t i = 1; i < v.size(); ++i){
		if(Vector2::Dot(v[i] - v[j], dir) > 0){
			j = i;
		}
	}
	return v[j];
}
Point2 Polygon::Nearest(const Point2 &p) const{
	size_t j = 0;
	coord_type dj = (p - v[j]).NormSq();
	for(size_t i = 1; i < v.size(); ++i){
		coord_type d = (p - v[i]).NormSq();
		if(d < dj){
			dj = d;
			j = i;
		}
	}
	return v[j];
}
AABB2 Polygon::Bounds() const{
	AABB2 b;
	for(size_t i = 0; i < v.size(); ++i){
		b += v[i];
	}
	return b;
}
	
coord_type Polygon::Perimeter() const{
	coord_type p = 0;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		p += (v[j] - v[i]).Norm();
	}
	return p;
}
coord_type Polygon::Area() const{
	coord_type a = 0;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		a += Vector2::Cross(v[i] - Point2::Origin(), v[j] - Point2::Origin());
	}
	return 0.5*a;
}
Point2 Polygon::Centroid() const{
	Vector2 c;
	for(size_t i = v.size()-1, j = 0; j < v.size(); i = j++){
		Vector2 m(Point2::Interpolate(v[i], v[j], 0.5) - Point2::Origin());
		coord_type x = Vector2::Cross(v[i] - Point2::Origin(), v[j] - Point2::Origin());
		c += x*m;
	}
	return Point2::Origin() + c/(3*Area());
}

complex_type Polygon::FourierTransform(const Vector2 &f) const{
	if(Vector2::Zero() == f){
		return Area();
	}
	complex_type z = 0;
	// For k != 0,
	//   S(k) = i/|k|^2 * \sum_{i=0}^{n-1} z.((v_{i+1}-v_{i}) x k) sinc(k.(v_{i+1}-v_{i})/2) e^{ik.(v_{i+1}+v_{i})/2}
	// where sinc(x) = sin(x)/x, and k = 2*pi*f.
	for(size_t p = v.size()-1, q = 0; q < v.size(); p = q++){
		const Vector2 u(v[q] - v[p]);
		const Point2 c(Point2::Interpolate(v[p], v[q], 0.5));

		const coord_type num = Vector2::Cross(u, f) * specfun::FourierSinc(Vector2::Dot(u, f));
		coord_type cs, sn;
		specfun::cossin(2*M_PI*Vector2::Dot(Point2::Origin() - c, f), &cs, &sn);

		z += num * complex_type(sn, -cs);
	}
	z /= 2*M_PI*f.NormSq();
	return z;
}

Polygon* Polygon::Offset(const coord_type &dist, const coord_type &tol) const{
	return NULL; // TODO
}


onedim::Parameterizable::chart_polyline Polygon::GetChartPolyline(index_type ichart, const coord_type &tol) const{
	onedim::Parameterizable::chart_polyline poly;
	poly.reserve(2);
	poly.push_back(vertex(ichart));
	poly.push_back(vertex(ichart+1));
	return poly;
}

bool Polygon::Triangulate(index_vector &triangle_indices) const{
	const size_t n = v.size();
	if(n < 3){ return false; }
	
	// Triangulation (without vertex insertion) will have n-2 triangles.
	triangle_indices.resize(3*(n-2));
	// We will store a working copy of the currently clipped polygon;
	// if it has m vertices currently, then we need to store 3 <= m <= n.
	// We also need to store the resulting triangles, requiring 3*(n-m) indices.
	// Together, the total size is 3*n - 2*m, which must be <= 3*(n-2). This is true for m >= 3.
	// Thus, we can keep the working copy of the polygon at the very end,
	// and add triangles to the beginning.

	// Let V be the working copy.
	index_type *V; // pointer to start of working copy
	size_t nv; // number of vertices in V
	size_t nt = 0; // number of triangles currently in triangulation
	
	// Make a copy of all the vertices
	V = &triangle_indices[2*n-6];
	nv = n;
	for(size_t i = 0; i < n; ++i){ V[i] = i; }
	
	index_type count = 2*nv;
	for(size_t i = nv-1; nv > 2; ){
		size_t u, w;
		if(0 >= (count--)){ // We have looped too much; this is a bad polygon
			return false;
		}
		// get 3 consecutive vertices
		u = i; //if(nv <= u){ u = 0; } // prev
		i = u+1; if(nv <= i){ i = 0; } // mid
		w = i+1; if(nv <= w){ w = 0; } // next

		// Determine if we can clip the ear
		bool can_clip = true;
		if(Point2::Orient(v[V[u]], v[V[i]], v[V[w]]) < 0){
			can_clip = false;
		}else{
			// if the u-i-w triangle contains any other vertex, can't clip.
			for(size_t p = 0; p < nv; ++p){
				if((p == u) || (p == i) || (p == w)){ continue; }
				if(
					(Point2::Orient(v[V[u]],v[V[i]],v[V[p]]) > 0) &&
					(Point2::Orient(v[V[i]],v[V[w]],v[V[p]]) > 0) &&
					(Point2::Orient(v[V[w]],v[V[u]],v[V[p]]) > 0)
				){
					can_clip = false;
					break;
				}
			}
		}

		// Perform the clip
		if(can_clip){
			index_type ii[3] = {V[u], V[i], V[w]};
			// erase vertex i
			while(i > 0){
				V[i] = V[i-1];
				--i;
			}
			++V; --nv; count = 2*nv;
			
			triangle_indices[3*nt+0] = ii[0];
			triangle_indices[3*nt+1] = ii[1];
			triangle_indices[3*nt+2] = ii[2];
			++nt;
		}
	}
	return true;
}

void Polygon::GetSamples(size_t n, Region::SamplingSpec spec, std::vector<point_type> &points) const{
	AABB2 bounds(Bounds());
	Rectangle r(bounds.Size(0), bounds.Size(1));
	std::vector<point_type> superset;
	r.GetSamples(n, spec, superset);
	points.reserve(superset.size());
	for(size_t i = 0; i < superset.size(); ++i){
		if(Contains(superset[i])){
			points.push_back(superset[i]);
		}
	}
}
