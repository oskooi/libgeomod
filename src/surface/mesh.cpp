#include <geomod/surface/mesh.h>

using namespace geomod;

affine::AABB3 surface::Mesh::Bounds() const{
	affine::AABB3 bounds;
	for(size_t i = 0; i < vert.size(); ++i){
		bounds += vert[i];
	}
	return bounds;
}
