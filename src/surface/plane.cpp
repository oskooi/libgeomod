#include <geomod/surface/plane.h>
#include <geomod/planar/region.h>
#include <geomod/planar/polygon.h>

using namespace geomod;

bool surface::Plane::RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const{
	// Solve: n.(r.p + t*r.d - p) == 0
	//        t = n.(p-r.p) / n.(r.d) 
	//const coord_type denom = affine::Vector3::Dot(r.dir(), affine::Vector3::Z());
	const coord_type denom = r.dir()[2];
	if(0 == denom){ return false; }
	//x.t = affine::Vector3::Dot(affine::Vector3::Z(), p - r.origin()) / denom;
	x.t = -r.origin()[2] / denom;
	x.p = r.origin() + x.t * r.dir();
	x.uv = affine::Point2(x.p[0], x.p[1]);
	return x.t > 0;
}

surface::Mesh* surface::Plane::AsMesh(const planar::Region *reg, const coord_type &tol) const{
	planar::Polygon *boundary = reg->AsPolygon();
	planar::Polygon::index_vector triangle_indices;
	boundary->Triangulate(triangle_indices);
	surface::Mesh::vertex_vector vertices;
	vertices.reserve(boundary->size());
	for(size_t i = 0; i < boundary->size(); ++i){
		vertices.push_back(surface::Mesh::vertex_type(
			(*boundary)[i][0],
			(*boundary)[i][1],
			0
		));
	}
	surface::Mesh *mesh = new surface::Mesh(vertices, triangle_indices);
	delete boundary;
	return mesh;
}
