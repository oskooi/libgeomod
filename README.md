# libgeomod: A library of geometric modeling for physical simulation.

libgeomod is a small self-contained geometry kernel designed for embedding within physical simulation tools. It handles many 2D operations on shapes and CAD geometry, and some basic 3D operations.


