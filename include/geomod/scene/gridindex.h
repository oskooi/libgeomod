#ifndef GEOMOD_SCENE_GRIDINDEX_H_
#define GEOMOD_SCENE_GRIDINDEX_H_

#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <vector>
#include <map>

namespace geomod{
namespace scene{

// This is a spatial index on points based on a uniform grid.
// Points are associated with an arbitrary data type, and can
// be inserted, removed, and queried against.
// Querying against points returns the data of all points within
// a given distance.

template <typename IndexType, typename DataType>
class GridIndex3{
	affine::AABB3 domain;

	typedef std::multimap<IndexType, std::pair<affine::Point3,DataType> > point_map;
	typedef std::pair<typename point_map::const_iterator, typename point_map::const_iterator> point_map_range;
	point_map m;

	// Each spatial dimension of the domain box is discretized into
	// nvals[dim] == 2^nbits[dim] bins.
	unsigned nbits[3], nvals[3];

	// The reciprocal of the size of the domain are in idim[dim].
	coord_type idim[3];

	void point_to_bin(const affine::Point3 &p, IndexType *bin) const{
		for(unsigned dim = 0; dim < 3; ++dim){
			coord_type t = (p[dim] - domain.Min()[dim]) * idim[dim];
			t *= nvals[dim];
			
			IndexType d = ((IndexType)t);
			if(d < 0){ d = 0; }
			else if(d >= nvals[dim]){ d = nvals[dim]-1; }

			bin[dim] = d;
		}
	}
	IndexType bin_to_key(const IndexType *bin) const{
		return bin[2] | (bin[1] << nbits[2]) | (bin[0] << (nbits[2]+nbits[1]));
	}
	IndexType point_to_key(const affine::Point3 &p) const{
		IndexType bin[3];
		point_to_bin(p, bin);
		return bin_to_key(bin);
	}
	void key_to_bin(IndexType key, IndexType *bin) const{
		bin[0] = (key >> (nbits[2]+nbits[1]));
		key &= ~(bin[0] << (nbits[2]+nbits[1]));
		bin[1] = (key >> nbits[2]);
		key &= ~(bin[1] << nbits[2]);
		bin[2] = key;
	}
public:
	GridIndex3(
		const affine::AABB3 &bounds,
		unsigned nbitsx,
		unsigned nbitsy,
		unsigned nbitsz
	):domain(bounds){
		nbits[0] = nbitsx; nvals[0] = (1 << nbits[0]);
		nbits[1] = nbitsy; nvals[1] = (1 << nbits[1]);
		nbits[2] = nbitsz; nvals[2] = (1 << nbits[2]);
		idim[0] = coord_type(1) / bounds.Size(0);
		idim[1] = coord_type(1) / bounds.Size(1);
		idim[2] = coord_type(1) / bounds.Size(2);

		//assert(nbits[0] + nbits[1] + nbits[2] < 8*sizeof(IndexType));
	}

	void insert(const affine::Point3 &p, const DataType &data){
		m.insert(typename point_map::value_type(point_to_key(p), typename point_map::mapped_type(p, data)));
	}
	void remove(const affine::Point3 &p, const DataType &data){
		IndexType key = point_to_key(p);
		point_map_range range = m.equal_range(key);
		for(typename point_map::const_iterator i = range.first; range.second != i; ++i){
			if(i->second.first == p && i->second.second == data){
				m.erase(i);
				break;
			}
		}
	}

	void query(const affine::Point3 &p, const coord_type &dist, std::vector<DataType> &results) const{
		IndexType bin[3];
		point_to_bin(p, bin);
		const int ns[3] = {
			int(dist * nvals[0] * idim[0])+1,
			int(dist * nvals[1] * idim[1])+1,
			int(dist * nvals[2] * idim[2])+1
		};
		const IndexType i0[3] = {
			bin[0] >= ns[0] ? bin[0]-ns[0] : 0,
			bin[1] >= ns[1] ? bin[1]-ns[1] : 0,
			bin[2] >= ns[2] ? bin[2]-ns[2] : 0
		};
		const IndexType i1[3] = {
			bin[0]+ns[0] < nvals[0] ? bin[0]+ns[0] : nvals[0]-1,
			bin[1]+ns[1] < nvals[1] ? bin[1]+ns[1] : nvals[1]-1,
			bin[2]+ns[2] < nvals[2] ? bin[2]+ns[2] : nvals[2]-1
		};

		for(IndexType ix = i0[0]; ix <= i1[0]; ++ix){
			for(IndexType iy = i0[1]; iy <= i1[1]; ++iy){
				for(IndexType iz = i0[2]; iz <= i1[2]; ++iz){
					IndexType b[3] = {ix, iy, iz};
					IndexType key = bin_to_key(b);
					point_map_range range = m.equal_range(key);
					for(typename point_map::const_iterator i = range.first; range.second != i; ++i){
						affine::Vector3 v(p - i->second.first);
						if(v.NormInf() < dist){
							results.push_back(i->second.second);
						}
					}
				}
			}
		}
	}
};


} // namespace scene
} // namespace geomod

#endif // GEOMOD_SCENE_GRIDINDEX_H_
