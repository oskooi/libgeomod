#ifndef GEOMOD_TYPES_H_
#define GEOMOD_TYPES_H_

#ifndef NOMINMAX
#define NOMINMAX
#endif

#define _USE_MATH_DEFINES
#include <cmath>
#include <complex>
#include <cstdint>
#include <cstddef>
#include <limits>

namespace geomod {

typedef double coord_type;   // spatial coordinates
typedef std::complex<coord_type> complex_type; // complex fields
typedef int32_t index_type;

} // namespace geomod

#endif  // GEOMOD_TYPES_H_
