#ifndef GEOMOD_VOLUME_EXTRUSION_H_
#define GEOMOD_VOLUME_EXTRUSION_H_

#include <vector>
#include <complex>
#include <geomod/volume/solid.h>
#include <geomod/planar/region.h>

namespace geomod{
namespace volume{

class Extrusion : public Solid{
public:
	typedef affine::Point3  point_type;
	typedef affine::Vector3 vector_type;
	typedef affine::AABB3   bound_type;
	typedef planar::Region  region_type;
private:
	class BaseParameterization : public surface::Parameterization{
		const Extrusion *e;
		bool z1;
	public:
		BaseParameterization(const Extrusion *parent, bool z1): e(parent), z1(z1) {}
		affine::Point3 operator()(const affine::Point2 &uv) const;
		affine::Point2 operator()(const affine::Point3 &r) const;
		bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
		void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;
		affine::AABB2 Bounds() const;
		uv_boundary GetBoundary(const coord_type &tol = 1e-3) const;
	};
	class EdgeParameterization : public surface::Parameterization{
		const Extrusion *e;
		index_type ichart;
	public:
		EdgeParameterization(const Extrusion *parent, index_type ichart): e(parent), ichart(ichart) {}
		affine::Point3 operator()(const affine::Point2 &uv) const;
		affine::Point2 operator()(const affine::Point3 &r) const;
		bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const;
		void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const;
		affine::AABB2 Bounds() const;
		uv_boundary GetBoundary(const coord_type &tol = 1e-3) const;
	};
	region_type *reg;
	vector_type h;
	BaseParameterization param0, param1;
	std::vector<EdgeParameterization> parame;
public:
	Extrusion(const region_type *reg, const vector_type &v);
	~Extrusion();

	const region_type* base() const{ return reg; }
	const vector_type& offset() const{ return h; }

	coord_type SurfaceArea() const;
	coord_type Volume() const;
	point_type Centroid() const;
	
	point_type Extremum(const vector_type &dir) const;
	point_type Nearest(const point_type &p) const;
	bound_type Bounds() const;

	size_t NumCharts() const;
	const surface::Parameterization* GetChartParameterization(index_type ichart) const;
	surface::Parameterizable::chart_outline GetChartOutline(index_type ichart, const coord_type &tol = 1e-3) const;

	Extrusion* Clone() const;

	bool Contains(const point_type &p) const;
	bool Convex() const;

	int EulerCharacteristic() const{ return 2; }

	surface::Mesh* AsMesh(const coord_type &tol = 1e-3) const;
	Solid* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace volume
} // namespace geomod

#endif // GEOMOD_VOLUME_EXTRUSION_H_
