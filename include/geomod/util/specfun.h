#ifndef GEOMOD_UTIL_SPECFUN_H_
#define GEOMOD_UTIL_SPECFUN_H_

#include <geomod/types.h>

namespace geomod {
namespace specfun {

template <typename T>
inline T clamp(T x){
	if(x < T(0)){ x = T(0); }
	else if(x > T(1)){ x = T(1); }
	return x;
}

// Compute cosine and sine simultaneously.
inline void cossin(const coord_type &x, coord_type *c, coord_type *s){
#ifdef  __GNU_LIBRARY__
	sincos(x, s, c);
#else
	*c = std::cos(x); *s = std::sin(x);
#endif
}

// Compute cosine and sine simultaneously for degree arguments.
// Special argument values are special cased exactly.
inline void cossind(coord_type x, coord_type *c, coord_type *s){
	const coord_type h(0.5);
	const coord_type r2 = std::sqrt(coord_type(0.5));
	const coord_type r3 = std::sqrt(coord_type(0.75));
	static coord_type special_angles[][3] = {
		// Multiples of 90
		{    0,  1,  0 },
		{   90,  0,  1 },
		{  -90,  0, -1 },
		{  180, -1,  0 },
		{ -180, -1,  0 },
		// 45s
		{   45,  r2,  r2 },
		{  -45,  r2, -r2 },
		{  135, -r2,  r2 },
		{ -135, -r2, -r2 },
		// 30s
		{   30,  r3,   h },
		{  -30,  r3,  -h },
		{   60,   h,  r3 },
		{  -60,   h, -r3 },
		{  120,  -h,  r3 },
		{ -120,  -h, -r3 },
		{  150, -r3,   h },
		{ -150, -r3,  -h },
	};

	// Argument wrap
	if (x > 180) {
		x -= 360;
	}else if (x < -180) {
		x += 360;
	}

	for(unsigned i = 0; i < sizeof(special_angles)/(3*sizeof(coord_type)); ++i){
		if(x == special_angles[i][0]){
			*c = special_angles[i][1];
			*s = special_angles[i][2];
			return;
		}
	}

	const coord_type r = x * (M_PI/180.);
#ifdef  __GNU_LIBRARY__
	sincos(r, s, c);
#else
	*c = std::cos(r); *s = std::sin(r);
#endif

}

// Defined as Sin[pi x] / (pi x)
template <typename T>
T FourierSinc(T x){
	static const T zero(0);
	static const T one(1);
	static const T sixth(one/T(6));
	static const T half(one/T(2));
	T intpart;
	// Handle integer case
	if(zero == x){ return one; }
	if(x < zero){ x = -x; }
	const T fracpart = modf(x, &intpart);
	if(zero == fracpart){
		return zero;
	}
	// Handle the half-integer case
	if(half == fracpart){
		T a = M_2_PI / (1+2*intpart);
		if(0 != ((int)(intpart)) % 2){
			a = -a;
		}
		return a;
	}
	// Handle the small argument case
	x *= M_PI;
	if(x < 2.44140625e-4){
		return one - sixth*x*x;
	}
	// Handle the general case
	return std::sin(x)/x;
}

// Defined as 2*BesselJ[1, 2 pi x] / (2 pi x)
template <typename T>
T FourierJinc(T x){
	static const T zero(0);
	static const T one(1);
	static const T eighth(one/T(8));
	
	if(zero == x){ return one; }
	if(x < zero){ x = -x; }
	x *= (2*M_PI);
	if(x < 5e-5){
		return one - eighth*x*x;
	}
	// The following is from crbond.com
	static const double a1[] = {
		 0.1171875,
		-0.1441955566406250,
		 0.6765925884246826,
		-6.883914268109947,
		 1.215978918765359e2,
		-3.302272294480852e3,
		 1.276412726461746e5,
		-6.656367718817688e6,
		 4.502786003050393e8,
		-3.833857520742790e10,
		 4.011838599133198e12,
		-5.060568503314727e14,
		 7.572616461117958e16,
		-1.326257285320556e19};
	static const double b1[] = {
		-0.1025390625,
		 0.2775764465332031,
		-1.993531733751297,
		 2.724882731126854e1,
		-6.038440767050702e2,
		 1.971837591223663e4,
		-8.902978767070678e5,
		 5.310411010968522e7,
		-4.043620325107754e9,
		 3.827011346598605e11,
		-4.406481417852278e13,
		 6.065091351222699e15,
		-9.833883876590679e17,
		 1.855045211579828e20};

	if(x <= 12.0){
		double x2 = x*x;
		double j1 = 1.0;
		double r = 1.0;
		int k;
		for(k=1;k<=30;k++){
			r *= -0.25*x2/(k*(k+1));
			j1 += r;
			if (fabs(r) < fabs(j1)*1e-15) break;
		}
		return j1;
	}else{
		int kz;
		if (x >= 50.0) kz = 8;          /* Can be changed to 10 */
		else if (x >= 35.0) kz = 10;    /*  "       "        12 */
		else kz = 12;                   /*  "       "        14 */
		double cu = sqrt(M_2_PI/x);
		double t2 = x-0.75*M_PI;
		double p1 = 1.0;
		double q1 = 0.375/x;
		int k;
		for(k=0;k<kz;k++){
			p1 += a1[k]*pow(x,-2*k-2);
			q1 += b1[k]*pow(x,-2*k-3);
		}
		return 2.0*cu*(p1*cos(t2)-q1*sin(t2))/x;
	}
}

} // namespace specfun
} // namespace geomod

#endif  // GEOMOD_UTIL_SPECFUN_H_
