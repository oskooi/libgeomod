#ifndef GEOMOD_UTIL_CLONABLE_H_
#define GEOMOD_UTIL_CLONABLE_H_

namespace geomod {
namespace util {

// Interface class for types that can be cloned.

class Clonable{
public:
	virtual ~Clonable(){}

	virtual Clonable* Clone() const = 0;
};

} // namespace util
} // namespace geomod

#endif  // GEOMOD_UTIL_CLONABLE_H_
