#ifndef GEOMOD_PLANAR_PIECEWISE_H_
#define GEOMOD_PLANAR_PIECEWISE_H_

#include <geomod/types.h>
#include <geomod/planar/region.h>

// The following are needed to for the conversion constructors
#include <geomod/planar/circle.h>
#include <geomod/planar/ellipse.h>
#include <geomod/planar/rectangle.h>
#include <geomod/planar/polygon.h>

namespace geomod{
namespace planar{

// This represents a planar, simply connected region.
// It is assumed to have piecewise smooth boundary.

class Piecewise:
	public Region
{
public:
	typedef affine::Point2 point_type;
	typedef affine::Vector2 vector_type;
	typedef affine::Ray2 ray_type;

	// A piecewise-defined region is composed of a (closed loop) sequence of
	// Segment objects. Each segment is a curve that can be smoothly
	// parameterized.
	class Segment : public util::Clonable, onedim::Parameterization{
	public:
		virtual ~Segment(){}

		// onedim::Parameterization methods
		virtual affine::Point2 operator()(const coord_type &u) const = 0;
		virtual coord_type operator()(const affine::Point2 &p)  const = 0;
		virtual bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const = 0;
		virtual void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const = 0;
		virtual onedim::Interval Bounds() const = 0;

		virtual Segment* Clone() const = 0;
		virtual coord_type Extremum(const vector_type &dir) const = 0;
		virtual coord_type Extremum(const point_type &p) const = 0;
		virtual void Discretize(const coord_type &tol, std::vector<point_type> &p) const = 0;
		virtual void Offset(const coord_type &dist, const coord_type &tol, std::vector<Segment*> &seg) const = 0;
	};

	class Line : public Segment{
		point_type a, b;
	public:
		Line(const point_type &start_point, const point_type &end_point):
			a(start_point),
			b(end_point)
		{
		}

		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const;

		Line* Clone() const;
		coord_type Extremum(const vector_type &dir) const;
		coord_type Extremum(const point_type &p) const;
		void Discretize(const coord_type &tol, std::vector<point_type> &p) const;
		void Offset(const coord_type &dist, const coord_type &tol, std::vector<Segment*> &seg) const;
	};

	class Arc : public Segment{
		point_type a, b;
		coord_type g; // bulge parameter
	public:
		Arc(const point_type &start_point, const point_type &end_point, const coord_type &bulge):
			a(start_point),
			b(end_point),
			g(bulge)
		{
		}

		Arc(const point_type &start_point, const point_type &end_point, const point_type &thru_point);

		const coord_type& bulge() const{ return g; }
		const coord_type& angle() const;
		const coord_type& radius() const;

		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const;

		Arc* Clone() const;
		coord_type Extremum(const vector_type &dir) const;
		coord_type Extremum(const point_type &p) const;
		void Discretize(const coord_type &tol, std::vector<point_type> &p) const;
		void Offset(const coord_type &dist, const coord_type &tol, std::vector<Segment*> &seg) const;
	};

	class Conic : public Segment{
		point_type a, b;
		point_type c;
		coord_type w;
	public:
		Conic(
			const point_type &start_point,
			const point_type &end_point,
			const point_type &control_point,
			const coord_type &control_weight
		):
			a(start_point),
			b(end_point),
			c(control_point),
			w(control_weight)
		{
		}

		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const;

		Conic* Clone() const;
		coord_type Extremum(const vector_type &dir) const;
		coord_type Extremum(const point_type &p) const;
		void Discretize(const coord_type &tol, std::vector<point_type> &p) const;
		void Offset(const coord_type &dist, const coord_type &tol, std::vector<Segment*> &seg) const;
	};

	class BSpline : public Segment{
	public:
		typedef std::vector<point_type>     control_point_vector;
		typedef std::vector<coord_type> knot_vector;
		typedef std::vector<coord_type> weight_vector;
	private:
		control_point_vector c;
		knot_vector knot;
		weight_vector w;
	public:
		BSpline(
			const control_point_vector &control_points,
			const knot_vector &knots,
			const weight_vector &weights = weight_vector()
		):
			c(control_points),
			knot(knots),
			w(weights)
		{
		}

		index_type degree() const;
		index_type order() const;

		bool Rational() const;

		affine::Point2 operator()(const coord_type &u) const;
		coord_type operator()(const affine::Point2 &p)  const;
		bool RayIntersection(const affine::Ray2 &r, std::vector<affine::Ray2::Intersection> &x) const;
		void Neighborhood(const coord_type &u, onedim::Parameterization::DifferentialGeometry &dg) const;
		onedim::Interval Bounds() const;

		BSpline* Clone() const;
		coord_type Extremum(const vector_type &dir) const;
		coord_type Extremum(const point_type &p) const;
		void Discretize(const coord_type &tol, std::vector<point_type> &p) const;
		void Offset(const coord_type &dist, const coord_type &tol, std::vector<Segment*> &seg) const;
	};

	typedef std::vector<Segment*> segment_vector;

private:
	segment_vector seg;
	const Segment& segment(index_type i) const{
		const index_type n = seg.size();
		if(i < 0){ i += n; }
		else if(i >= n){ i -= n; }
		return *(seg[i]);
	}
public:
	Piecewise(const segment_vector &segments):seg(segments){}
	// Conversion from other primitive shapes to a piecewise-defined shape
	Piecewise(const Circle &circle);
	Piecewise(const Ellipse &ellipse);
	Piecewise(const Polygon &polygon);
	Piecewise(const Rectangle &rectangle);

	~Piecewise();

	Piecewise* Clone() const;

	bool Contains(const point_type &p) const;
	
	bool Convex() const;

	size_t NumCharts() const{ return seg.size(); }
	const onedim::Parameterization* GetChartParameterization(index_type ichart) const;
	
	affine::Point2 Extremum(const affine::Vector2 &dir) const;
	affine::Point2 Nearest(const affine::Point2 &p) const;
	affine::AABB2 Bounds() const;

	coord_type Perimeter() const;
	coord_type Area() const;
	affine::Point2 Centroid() const;

	const Region::FourierTransformable* GetFourierTransformable() const{ return NULL; }
	//complex_type FourierTransform(const affine::Vector2 &f) const;

	void GetSamples(size_t n, Region::SamplingSpec spec, std::vector<point_type> &points) const;

	Polygon* AsPolygon(const coord_type &tol = 1e-3) const;
	Piecewise* Offset(const coord_type &dist, const coord_type &tol = 0) const;
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_PIECEWISE_H_
