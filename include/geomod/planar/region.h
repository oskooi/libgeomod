#ifndef GEOMOD_PLANAR_REGION_H_
#define GEOMOD_PLANAR_REGION_H_

#include <vector>
#include <complex>
#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/aabb.h>
#include <geomod/onedim/parameterization.h>
#include <geomod/util/clonable.h>

namespace geomod{
namespace planar{

// This represents a planar, simply connected region.
// It is assumed to have piecewise smooth boundary.

class Polygon;

class Region:
	public onedim::Parameterizable,
	public util::Clonable
{
public:
	typedef affine::Point2  point_type;
	typedef affine::Vector2 vector_type;
	typedef affine::AABB2   bound_type;

	class FourierTransformable{
	public:
		virtual ~FourierTransformable(){}
		virtual complex_type FourierTransform(const affine::Vector2 &f) const = 0;
	};

	// Integrability
	virtual coord_type Perimeter() const = 0;
	virtual coord_type Area() const = 0;
	virtual point_type Centroid() const = 0;

	// Extremizability
	virtual point_type Extremum(const vector_type &dir) const = 0;
	virtual point_type Nearest(const point_type &p) const = 0;
	virtual bound_type Bounds() const = 0;
	
	// Parameterizability
	virtual size_t NumCharts() const = 0;
	virtual const onedim::Parameterization* GetChartParameterization(index_type ichart) const = 0;
	
	enum SamplingSpec{
		SAMPLED_APPROX, // no real guarantees about quality, but points will be roughly uniformly distributed
		SAMPLED_QUALITY, // Exploit symmetry of the domain, or use CVT, if possible. Falls back to APPROX otherwise.
		SAMPLED_RANDOM // Uses random rejection sampling
	};
	// Gets at most n points roughly uniformly distributed within the region
	virtual void GetSamples(size_t n, SamplingSpec spec, std::vector<point_type> &points) const = 0;

	virtual ~Region(){}

	Region* Clone() const = 0;

	virtual bool Contains(const point_type &p) const = 0;
	virtual bool Convex() const{ return false; }
	
	virtual const FourierTransformable* GetFourierTransformable() const{ return NULL; }
	
	virtual Polygon* AsPolygon(const coord_type &tol = 1e-3) const = 0;
	virtual Region* Offset(const coord_type &dist, const coord_type &tol = 0) const{ return NULL; }
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_SHAPE_H_
