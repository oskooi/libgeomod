#ifndef GEOMOD_PLANAR_GRID_H_
#define GEOMOD_PLANAR_GRID_H_

#include <geomod/types.h>
#include <geomod/affine/point.h>
#include <geomod/planar/lattice.h>
#include <vector>
#include <cfloat>

namespace geomod{
namespace planar{

class Grid{
public:
	typedef affine::Point2  point_type;
	
	virtual ~Grid(){}

	virtual index_type NumPoints() const = 0;
	virtual point_type GetPoint(index_type i) const = 0;
};

class RegularGrid : public Grid{
	Lattice L;
	point_type org;
	index_type nu, nv;
public:
	RegularGrid(
		const Lattice &discretization,
		const point_type &origin,
		index_type nu, index_type nv = 1
	):
		L(discretization),
		org(origin),
		nu(nu),
		nv(nv)
	{
	}

	const Lattice &lattice() const{ return L; }
	point_type origin() const{ return org; }
	void NumPoints(index_type &m, index_type &n) const{ m = nu; n = nv; }

	index_type NumPoints() const{ return nu*nv; }

	point_type GetPoint(index_type i) const{
		const index_type iu = i % nu;
		const index_type iv = i / nu;
		return org + iu * L.u() + iv * L.v();
	}
};

class HexapolarGrid : public Grid{
	index_type nr;
	coord_type maxr;
public:
	HexapolarGrid(
		const coord_type &radius,
		index_type nrings,
		bool inset = false
	):
		nr(nrings)
	{
		if(inset){
			maxr = radius*((coord_type)(2*nrings - 2) / (coord_type)(2*nrings - 1));
		}else{
			maxr = radius;
		}
	}

	index_type num_rings() const{ return nr; }
	coord_type max_radius() const{ return maxr; }

	index_type NumPoints() const{ return 3*nr*(nr-1) + 1; }

	point_type GetPoint(index_type i) const{
		if(0 == i){ return point_type::Origin(); }
		const index_type ir_prev = floor(sqrt((i - 0.25)/3) - 0.5 + DBL_EPSILON);
		const index_type ir = ir_prev + 1; // ring index we are currently in
		const index_type nq = 6*ir; // number of points in this ring
		const index_type iq = i - (3*ir_prev*(ir_prev-1) + 1);
		const coord_type q = ((coord_type)iq / (coord_type)nq) * 2*M_PI;
		const coord_type cs = std::cos(q);
		const coord_type sn = std::sin(q);
		const coord_type rr = ((coord_type)ir / (coord_type)(nr-1)) * maxr;
		return point_type(rr*cs, rr*sn);
	}
};

} // namespace planar
} // namespace geomod

#endif // GEOMOD_PLANAR_GRID_H_
