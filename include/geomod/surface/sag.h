#ifndef GEOMOD_SURFACE_SAG_H_
#define GEOMOD_SURFACE_SAG_H_

#include <geomod/types.h>
#include <geomod/affine/ray.h>
#include <geomod/planar/region.h>
#include <geomod/surface/surface.h>

namespace geomod{
namespace surface{

class Sag : public Surface{
public:
	class Function{
	public:
		virtual ~Function(){}
		virtual coord_type Evaluate(const affine::Point2 &p, affine::Vector2 &grad) const = 0;
	};
	const Function *func;
	const planar::Region *reg;
public:
	Sag(const planar::Region *region, const Function *func);
	
	bool Intersect(const affine::Ray3 &r, affine::Ray3::Intersection &x) const;
	coord_type Orient(const affine::Point3 &q) const;
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_SAG_H_
