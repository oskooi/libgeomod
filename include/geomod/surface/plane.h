#ifndef GEOMOD_SURFACE_PLANE_H_
#define GEOMOD_SURFACE_PLANE_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>
#include <geomod/affine/point.h>
#include <geomod/affine/ray.h>
#include <geomod/surface/surface.h>

namespace geomod{
namespace surface{

class Plane : public Surface{
public:
	Plane(){}

	bool RayIntersection(const affine::Ray3 &r, affine::Ray3::Intersection &x) const;

	Mesh* AsMesh(const planar::Region *reg, const coord_type &tol = 1e-3) const;

	coord_type Orient(const affine::Point3 &q) const{
		return q[2];
	}

	affine::Point3 operator()(const affine::Point2 &uv) const{ return affine::Point3(uv[0], uv[1], 0); }
	affine::Point2 operator()(const affine::Point3 &r) const{ return affine::Point2(r[0], r[1]); };
	bool RayIntersection(const affine::Ray3 &r, std::vector<affine::Ray3::Intersection> &x) const{
		affine::Ray3::Intersection i;
		if(RayIntersection(r, i)){
			x.push_back(i);
			return true;
		}
		return false;
	}
	void Neighborhood(const affine::Point2 &uv, DifferentialGeometry &dg) const{
		dg.dpdu = affine::Vector3::X();
		dg.dpdv = affine::Vector3::Y();
		dg.II[0] = 1;
		dg.II[1] = 0;
		dg.II[2] = 1;
		dg.orientation = 1;
	}

	Plane* Clone() const{ return new Plane(); }
};

} // namespace surface
} // namespace geomod

#endif // GEOMOD_SURFACE_PLANE_H_
