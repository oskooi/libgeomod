#ifndef GEOMOD_AFFINE_POINT_H_
#define GEOMOD_AFFINE_POINT_H_

#include <geomod/types.h>
#include <geomod/affine/vector.h>

namespace geomod{
namespace affine{

// A point defines a location in space.
// In homogeneous coordinates, a 2D point has the form (x, y, 1).

template <typename T>
class TPoint2{
public:
	typedef T value_type;
private:
	value_type p[2];
public:
	// Default constructor
	TPoint2(){
		p[0] = 0; p[1] = 0;
	}
	TPoint2(const value_type &x, const value_type &y){
		p[0] = x;
		p[1] = y;
	}

	static TPoint2 Origin(){
		static const TPoint2 O(value_type(0), value_type(0));
		return O;
	}
	static TPoint2 Infinity(){
		static const TPoint2 I(
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity()
		);
		return I;
	}
	value_type x() const{ return p[0]; }
	value_type y() const{ return p[1]; }
	const value_type &operator[](unsigned i) const{ return p[i]; }
	value_type &operator[](unsigned i){ return p[i]; }

	unsigned MinDim() const{
		unsigned i = 0;
		if(p[1] < p[i]){ i = 1; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(p[1] > p[i]){ i = 1; }
		return i;
	}

	TPoint2 operator+(const Vector2 &v) const{
		return TPoint2(p[0] + v[0], p[1] + v[1]);
	}
	TPoint2& operator+=(const Vector2 &v){
		p[0] += v[0];
		p[1] += v[1];
		return *this;
	}
	TPoint2 operator-(const Vector2 &v) const{
		return TPoint2(p[0] - v[0], p[1] - v[1]);
	}
	TPoint2& operator-=(const Vector2 &v){
		p[0] -= v[0];
		p[1] -= v[1];
		return *this;
	}
	Vector2 operator-(const TPoint2 &b) const{
		return Vector2(p[0] - b[0], p[1] - b[1]);
	}

	static TPoint2 Interpolate(const TPoint2 &a, const TPoint2 &b, const value_type &t){
		value_type t1(value_type(1) - t);
		return TPoint2(
			t1 * a[0] + t * b[0],
			t1 * a[1] + t * b[1]
		);
	}

	// Returns the relative orientation of three points in a plane.
	// The result is > 0 if c is to the left of the line standing
	// at a looking towards b, < 0 if to the right, and 0 if the points
	// are collinear.
	static value_type Orient(const TPoint2 &a, const TPoint2 &b, const TPoint2 &c){
		Vector2 ab(b - a);
		Vector2 bc(c - b);
		return Vector2::Cross(ab, bc);
	}
};

template <typename T>
inline bool operator==(const TPoint2<T> &a, const TPoint2<T> &b){
	return a[0] == b[0] && a[1] == b[1];
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TPoint2<T> &p){
	s << '(' << p[0] << ',' << p[1] << ')';
	return s;
}


template <typename T>
class TPoint3{
public:
	typedef T value_type;
private:
	value_type p[3];
public:
	TPoint3(){
		p[0] = 0; p[1] = 0; p[2] = 0;
	}
	TPoint3(const value_type &x, const value_type &y, const value_type &z){
		p[0] = x;
		p[1] = y;
		p[2] = z;
	}
	TPoint3(const TPoint2<value_type> &p2, const value_type &z){ p[0] = p2[0]; p[1] = p2[1]; p[2] = z; }

	static TPoint3 Origin(){
		static const TPoint3 O(value_type(0), value_type(0), value_type(0));
		return O;
	}
	static TPoint3 Infinity(){
		static const TPoint3 I(
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity()
		);
		return I;
	}
	value_type x() const{ return p[0]; }
	value_type y() const{ return p[1]; }
	value_type z() const{ return p[2]; }
	const value_type &operator[](unsigned i) const{ return p[i]; }
	value_type &operator[](unsigned i){ return p[i]; }

	unsigned MinDim() const{
		unsigned i = 0;
		if(p[1] < p[i]){ i = 1; }
		if(p[2] < p[i]){ i = 2; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(p[1] > p[i]){ i = 1; }
		if(p[2] > p[i]){ i = 2; }
		return i;
	}

	TPoint3 operator+(const Vector3 &v) const{
		return TPoint3(p[0] + v[0], p[1] + v[1], p[2] + v[2]);
	}
	TPoint3& operator+=(const Vector3 &v){
		p[0] += v[0];
		p[1] += v[1];
		p[2] += v[2];
		return *this;
	}
	TPoint3 operator-(const Vector3 &v) const{
		return TPoint3(p[0] - v[0], p[1] - v[1], p[2] - v[2]);
	}
	TPoint3& operator-=(const Vector3 &v){
		p[0] -= v[0];
		p[1] -= v[1];
		p[2] -= v[2];
		return *this;
	}
	Vector3 operator-(const TPoint3 &b) const{
		return Vector3(p[0] - b[0], p[1] - b[1], p[2] - b[2]);
	}
	TPoint2<value_type> Projection() const{
		return TPoint2<value_type>(p[0] / p[2], p[1] / p[2]);
	}
	static TPoint3 Interpolate(const TPoint3 &a, const TPoint3 &b, const value_type &t){
		value_type t1(value_type(1) - t);
		return TPoint3(
			t1 * a[0] + t * b[0],
			t1 * a[1] + t * b[1],
			t1 * a[2] + t * b[2]
		);
	}

	// Returns the relative orientation of four points in space.
	// The result is > 0 if d is on the positive side of the oriented
	// plane defined by (a, b, c), < 0 if on the negative side, and 0
	// if the points are coplanar.
	// The positive side of the oriented plane (a,b,c) is the side
	// towards which (b-a) x (c-a) points.
	static value_type Orient(const TPoint3 &a, const TPoint3 &b, const TPoint3 &c, const TPoint3 &d){
		const Vector3 ab(b - a);
		const Vector3 ac(c - a);
		const Vector3 ad(d - a);
		return Vector3::Dot(ab, Vector3::Cross(ac, ad));
	}
};

template <typename T>
inline bool operator==(const TPoint3<T> &a, const TPoint3<T> &b){
	return a[0] == b[0] && a[1] == b[1] && a[2] == b[2];
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TPoint3<T> &p){
	s << '(' << p[0] << ',' << p[1] << ',' << p[2] << ')';
	return s;
}

template <typename T>
class TPoint4{
public:
	typedef T value_type;
private:
	value_type p[4];
public:
	TPoint4(){
		p[0] = 0; p[1] = 0; p[2] = 0; p[3] = 0;
	}
	TPoint4(const value_type &x, const value_type &y, const value_type &z, const value_type &w){
		p[0] = x;
		p[1] = y;
		p[2] = z;
		p[3] = w;
	}
	TPoint4(const TPoint3<value_type> &p3, const value_type &w){ p[0] = p3[0]; p[1] = p3[1]; p[2] = p3[2]; p[3] = w; }

	static TPoint4 Origin(){
		static const TPoint4 O(value_type(0), value_type(0), value_type(0), value_type(0));
		return O;
	}
	static TPoint4 Infinity(){
		static const TPoint4 I(
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity(),
			std::numeric_limits<value_type>::infinity()
		);
		return I;
	}
	value_type x() const{ return p[0]; }
	value_type y() const{ return p[1]; }
	value_type z() const{ return p[2]; }
	value_type w() const{ return p[3]; }
	const value_type &operator[](unsigned i) const{ return p[i]; }
	value_type &operator[](unsigned i){ return p[i]; }

	unsigned MinDim() const{
		unsigned i = 0;
		if(p[1] < p[i]){ i = 1; }
		if(p[2] < p[i]){ i = 2; }
		if(p[3] < p[i]){ i = 3; }
		return i;
	}
	unsigned MaxDim() const{
		unsigned i = 0;
		if(p[1] > p[i]){ i = 1; }
		if(p[2] > p[i]){ i = 2; }
		if(p[3] > p[i]){ i = 3; }
		return i;
	}

	TPoint4 operator+(const Vector4 &v) const{
		return TPoint4(p[0] + v[0], p[1] + v[1], p[2] + v[2], p[3] + v[3]);
	}
	TPoint4& operator+=(const Vector4 &v){
		p[0] += v[0];
		p[1] += v[1];
		p[2] += v[2];
		p[3] += v[3];
		return *this;
	}
	TPoint4 operator-(const Vector4 &v) const{
		return TPoint4(p[0] - v[0], p[1] - v[1], p[2] - v[2], p[3] - v[3]);
	}
	TPoint4& operator-=(const Vector4 &v){
		p[0] -= v[0];
		p[1] -= v[1];
		p[2] -= v[2];
		p[3] -= v[3];
		return *this;
	}
	Vector4 operator-(const TPoint4 &b) const{
		return Vector4(p[0] - b[0], p[1] - b[1], p[2] - b[2], p[3] - b[3]);
	}
	TPoint3<value_type> Projection() const{
		return TPoint3<value_type>(p[0] / p[3], p[1] / p[3], p[2] / p[3]);
	}
	static TPoint4 Interpolate(const TPoint4 &a, const TPoint4 &b, const value_type &t){
		value_type t1(value_type(1) - t);
		return TPoint4(
			t1 * a[0] + t * b[0],
			t1 * a[1] + t * b[1],
			t1 * a[2] + t * b[2],
			t1 * a[3] + t * b[3]
		);
	}
};

template <typename T>
inline bool operator==(const TPoint4<T> &a, const TPoint4<T> &b){
	return a[0] == b[0] && a[1] == b[1] && a[2] == b[2] && a[3] == b[3];
}

template <typename T>
inline std::ostream& operator<<(std::ostream &s, const TPoint4<T> &p){
	s << '(' << p[0] << ',' << p[1] << ',' << p[2] << ',' << p[3] << ')';
	return s;
}

typedef TPoint2<coord_type> Point2;
typedef TPoint3<coord_type> Point3;
typedef TPoint4<coord_type> Point4;

} // namespace affine
} // namespace geomod

#endif  // GEOMOD_AFFINE_POINT_H_
