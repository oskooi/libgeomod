#ifndef GEOMOD_ONEDIM_INTERVAL_H_
#define GEOMOD_ONEDIM_INTERVAL_H_

#include <geomod/types.h>
#include <vector>

namespace geomod{
namespace onedim{

// This class represents a simply-connected region in 1D; an interval.

class Interval{
	coord_type a, b;
public:
	Interval(const coord_type &x0, const coord_type &x1):a(x0), b(x1){}

	const coord_type& beginning() const{ return a; }
	const coord_type& end()   const{ return b; }

	bool Contains(const coord_type &x) const{ return a <= x && x < b; }

	coord_type Center() const{
		return 0.5*(a+b);
	}
	coord_type Size() const{
		return b-a;
	}

	bool Empty() const{
		return (b <= a);
	}

	static void AsBreakpoints(
		const std::vector<Interval> &intervals,
		const Interval &domain,
		std::vector< std::pair<coord_type, size_t> > &x
	);
};

} // namespace onedim
} // namespace geomod

#endif // GEOMOD_ONEDIM_INTERVAL_H_

