CXX = c++
CXXFLAGS = -std=c++17 -fPIC
CXXINC = -I./include

BUILD_DIR = build_output

SRCS = $(shell find src -name '*.cpp')
DIRS = $(shell find src -type d | sed 's/src/./g' ) 
OBJS = $(patsubst src/%.cpp, $(BUILD_DIR)/%.o, $(SRCS))

all: outputdirs lib tests

lib: $(OBJS)
	$(AR) -crs $(BUILD_DIR)/libgeomod.a $^

$(BUILD_DIR)/%.o: src/%.cpp
	$(CXX) -c $(CXXFLAGS) $(CXXINC) $< -o $@

tests: \
	$(BUILD_DIR)/test_quaternion \
	$(BUILD_DIR)/test_affine \
	$(BUILD_DIR)/test_transform \
	$(BUILD_DIR)/test_extrusion \
	$(BUILD_DIR)/test_gridindex \
	$(BUILD_DIR)/test_grid

$(BUILD_DIR)/test_quaternion: test/test_quaternion.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_affine: test/test_affine.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_transform: test/test_transform.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_extrusion: test/test_extrusion.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod
$(BUILD_DIR)/test_gridindex: test/test_gridindex.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@
$(BUILD_DIR)/test_grid: test/test_grid.cpp
	$(CXX) $(CXXFLAGS) $(CXXINC) $(TESTLIBS) $< -o $@ -L$(BUILD_DIR) -lgeomod

outputdirs:
	mkdir -p $(BUILD_DIR)
	for dir in $(DIRS); do mkdir -p $(BUILD_DIR)/$$dir; done
